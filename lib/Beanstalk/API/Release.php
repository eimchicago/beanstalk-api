<?php

/**
 * This file is part of the beanstalk-api package.
 *
 * (c) Mickey Freeman <mickey.freeman@eimchicago.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Beanstalk\API;

use Buzz\Client\ClientInterface as BuzzClientInterface;

/**
 * @author  Mickey Freeman <mickey.freeman@eimchicago.com>
 */
class Release extends Api
{  
  public function findAll($repositoryId = null, $page = null, $perPage = null)
  {
    if ($repositoryId !== null) {
      $endpoint = sprintf('%s/releases', $repositoryId);
    } else {
      $endpoint = 'releases';
    }
    
    if ($page !== null || $perPage !== null) {
      $page = intval($page);
      $perPage = intval($perPage) > 50 ? 50 : intval($perPage);
      $endpoint .= '.' . $this->getFormat() . sprintf('?page=%s&per_page=%s', $page, $perPage);
    }
    
    return $this->requestGet($endpoint);
  }
  
  public function find($repositoryId, $releaseId)
  {
    return $this->requestGet(sprintf('%s/releases/%s', $repositoryId, $releaseId));
  }
  
  public function create($repositoryId, $environmentId, array $params)
  {    
    $endpoint = sprintf('%s/releases.' . $this->getFormat() . '?environment_id=%s', $repositoryId, $environmentId);
    return $this->requestPost($endpoint, array('release' => $params));
  }
  
  public function retry($repositoryId, $releaseId)
  {
    return $this->requestPut(sprintf('%s/releases/%s/retry', $repositoryId, $releaseId));
  }
}
