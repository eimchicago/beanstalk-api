<?php

/**
 * This file is part of the beanstalk-api package.
 *
 * (c) Mickey Freeman <mickey.freeman@eimchicago.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Beanstalk\API\Http;

use Buzz\Client\ClientInterface as BuzzClientInterface;
use Buzz\Client\Curl;
use Buzz\Message\MessageInterface;
use Buzz\Message\RequestInterface;
use Buzz\Message\Request;
use Buzz\Message\Response;
use Beanstalk\API\Http\Listener\ListenerInterface;

/**
 * @author  Mickey Freeman <mickey.freeman@eimchicago.com>
 */
class Client implements ClientInterface
{
  /**
   * @var array
   */
  protected $options = array(
      'base_url'      => 'https://beanstalkapp.com/api',
      'format'        => 'json',
      'formats'       => array('json', 'xml'),    // supported response formats
      'user_agent'    => 'beanstalk-api-php/0.0.1 (https://bitbucket.org/eimchicago/beanstalk-api)',
      'timeout'       => 10,
      'verify_peer'   => false
  );

  /**
   * @var BuzzClientInterface
   */
  protected $client;

  /**
   * @var MessageInterface
   */
  private $lastRequest;

  /**
   * @var RequestInterface
   */
  private $lastResponse;

  /**
   * @var ListenerInterface[]
   */
  protected $listeners = array();

  public function __construct(array $options = array(), BuzzClientInterface $client = null)
  {
    $this->client   = (is_null($client)) ? new Curl : $client;
    $this->options  = array_merge($this->options, $options);

    $this->client->setTimeout($this->options['timeout']);
    $this->client->setVerifyPeer($this->options['verify_peer']);
  }

  /**
   * {@inheritDoc}
   */
  public function addListener(ListenerInterface $listener)
  {
    $this->listeners[$listener->getName()] = $listener;

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getListener($name)
  {
    if (!$this->isListener($name)) {
      throw new \InvalidArgumentException(sprintf('Unknown listener %s', $name));
    }

    return $this->listeners[$name];
  }

  /**
   * {@inheritDoc}
   */
  public function isListener($name)
  {
    return isset($this->listeners[$name]);
  }

  /**
   * {@inheritDoc}
   */
  public function get($endpoint, $params = array(), $headers = array())
  {
    if (is_array($params) AND count($params) > 0) {
      $endpoint   .= (strpos($endpoint, '?') === false ? '?' : '&').http_build_query($params, '', '&');
      $params     = array();
    }

    return $this->request($endpoint, $params, 'GET', $headers);
  }

  /**
   * {@inheritDoc}
   */
  public function post($endpoint, $params = array(), $headers = array())
  {
    return $this->request($endpoint, $params, 'POST', $headers);
  }

  /**
   * {@inheritDoc}
   */
  public function put($endpoint, $params = array(), $headers = array())
  {
    return $this->request($endpoint, $params, 'PUT', $headers);
  }

  /**
   * {@inheritDoc}
   */
  public function delete($endpoint, $params = array(), $headers = array())
  {
    return $this->request($endpoint, $params, 'DELETE', $headers);
  }

  /**
   * {@inheritDoc}
   */
  public function request($endpoint, array $params = array(), $method, array $headers = array())
  {
    // do not set base URL if a full one was provided
    if (false === strpos($endpoint, $this->getApiBaseUrl())) {
      $endpoint = $this->getApiBaseUrl().'/'.$endpoint;
    }
    
    $formatSet = false;
    foreach ($this->options['formats'] as $format) {
      if (strpos($endpoint, ".$format") !== false) {
        $formatSet = true;
      }
    }

    // change the response format
    if (!$formatSet) {
      $endpoint .=  '.' . $this->getResponseFormat();
    }

    $request = $this->createRequest($method, $endpoint);

    if (!empty($headers)) {
      $request->addHeaders($headers);
    }
    
    $request->addHeader('Content-type: application/' . $this->getResponseFormat());

    if (!empty($params)) {
      if ($this->getResponseFormat() == 'json' && is_array($params)) {
        $request->setContent(json_encode($params));
      } else if ($this->getResponseFormat() == 'xml' && is_array($params)) {
        $request->setContent($this->buildXMLRequest($params));
      } else {
        $request->setContent($params);
      }
    }

    $response = new Response;

    $this->executeListeners($request, 'preSend');

    $this->client->send($request, $response);

    $this->executeListeners($request, 'postSend', $response);

    $this->lastRequest  = $request;
    $this->lastResponse = $response;

    return $response;
  }
  
  protected function buildXMLRequest($params)
  {
    $document = new \SimpleXMLElement('<root/>');
    
    foreach ($params as $key => $entity) {
      $element = $document->addChild($key);
      foreach ($entity as $field => $value) {
        $element->addChild($field, $value);
      } // end foreach entity
    } // end foreach param
    
    $children = $document->children();
    $data = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
    foreach ($children as $child) {
      $data .= $child->asXML();
    }
    
    return $data;
  }

  /**
   * @access public
   * @return BuzzClientInterface
   */
  public function getClient()
  {
    return $this->client;
  }

  /**
   * {@inheritDoc}
   */
  public function getResponseFormat()
  {
    return $this->options['format'];
  }

  /**
   * {@inheritDoc}
   */
  public function setResponseFormat($format)
  {
    if (!in_array($format, $this->options['formats'])) {
      throw new \InvalidArgumentException(sprintf('Unsupported response format %s', $format));
    }

    $this->options['format'] = $format;

    return $this;
  }
  
  public function setOptions(array $options = array())
  {
    $this->options = array_merge($this->options, $options);
  }
  
  public function getOptions()
  {
    return $this->options;
  }

  /**
   * {@inheritDoc}
   */
  public function getApiBaseUrl()
  {
    return $this->options['base_url'];
  }
  
  public function setApiBaseUrl($url)
  {
    $this->options['base_url'] = $url;
  }

  /**
   * @access public
   * @return MessageInterface
   */
  public function getLastRequest()
  {
    return $this->lastRequest;
  }

  /**
   * @access public
   * @return RequestInterface
   */
  public function getLastResponse()
  {
    return $this->lastResponse;
  }

  /**
   * @access protected
   * @param  string           $method
   * @param  string           $url
   * @return RequestInterface
   */
  protected function createRequest($method, $url)
  {
    $request = new Request($method);
    $request->addHeaders(array(
            'User-Agent' => $this->options['user_agent']
        ));
    $request->setProtocolVersion(1.1);
    $request->fromUrl($url);

    return $request;
  }

  /**
   * Execute all available listeners
   *
   * $when can be: preSend or postSend
   *
   * @access protected
   * @param RequestInterface $request
   * @param string           $when     When to execute the listener
   * @param MessageInterface $response
   */
  protected function executeListeners(RequestInterface $request, $when = 'preSend', MessageInterface $response = null)
  {
    $haveListeners  = count($this->listeners) > 0;

    if (!$haveListeners) {
      return;
    }

    $params = array($request);

    if (!is_null($response)) {
      $params[] = $response;
    }

    foreach ($this->listeners as $listener) {
      call_user_func_array(array($listener, $when), $params);
    }
  }
}
