<?php

/**
 * This file is part of the bitbucket-api package.
 *
 * (c) Mickey Freeman <mickey.freeman@eimchicago.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Beanstalk\API\Http\Listener;

use Buzz\Listener\BasicAuthListener as BaseListener;

/**
 * @author  Mickey Freeman <mickey.freeman@eimchicago.com>
 */
class BasicAuthListener extends BaseListener implements ListenerInterface
{
  /**
   * {@inheritDoc}
   */
  public function getName()
  {
      return 'basicauth';
  }
} 
