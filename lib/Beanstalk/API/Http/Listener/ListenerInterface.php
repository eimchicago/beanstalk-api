<?php

/**
 * This file is part of the beanstalk-api package.
 *
 * (c) Mickey Freeman <mickey.freeman@eimchicago.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Beanstalk\API\Http\Listener;

use Buzz\Listener\ListenerInterface as BaseInterface;

/**
 * @author  Mickey Freeman <mickey.freeman@eimchicago.com>
 */
interface ListenerInterface extends BaseInterface
{
  /**
   * Get listener (unique) name
   *
   * @access public
   * @return string
   */
  public function getName();
} 
