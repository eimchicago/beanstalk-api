<?php

/**
 * This file is part of the beanstalk-api package.
 *
 * (c) Mickey Freeman <mickey.freeman@eimchicago.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Beanstalk\API;

use Buzz\Client\ClientInterface as BuzzClientInterface;

/**
 * @author  Mickey Freeman <mickey.freeman@eimchicago.com>
 */
class Node extends Api
{  
  public function find($repositoryId, $path = null, $revision = null, $size = false, $contents = false)
  {
    $addOns = array();
    
    if ($path !== null) {
      array_push($addOns, sprintf('path=%s', $path));
    }
    
    if ($revision !== null) {
      array_push($addOns, sprintf('revision=%s', $revision));
    }
    
    if ($size) {
      array_push($addOns, sprintf('size=%d', $size));
    }
    
    if ($contents) {
      array_push($addOns, sprintf('contents=%d', $contents));
    }
    $endpoint = sprintf('repositories/%s/node.' . $this->getFormat(), $repositoryId);
    $count = 0;
    foreach ($addOns as $addOn) {
      $endpoint .= ($count == 0) ? "?" . $addOn : '&' . $addOn;
      $count++;
    } // end foreach addon
    
    return $this->requestGet($endpoint);    
  }
  
  public function findBlob($repositoryId, $blobId, $name = null)
  {
    $addOn = null;
    if ($name !== null) {
      $addOn = sprintf('&name=%s', $name);
    }
    
    $endpoint = sprintf('repositories/%s/blob.' . $this->getFormat() . '?id=%s', $repositoryId, $blobId);
    return $this->requestGet($endpoint . $addOn);
  }
  
  public function create($repositoryId, array $params)
  {
    return $this->requestPost(sprintf('repositories/%s/node', $repositoryId), $params);
  }
  
  public function update($repositoryId, array $params)
  {    
    return $this->requestPut(sprintf('repositories/%s/node', $repositoryId), $params);
  }
  
  public function delete($repositoryId, array $params)
  {    
    return $this->requestDelete(sprintf('repositories/%s/node', $repositoryId), $params);
  }
  
  public function rename($repositoryId, array $params)
  {    
    return $this->requestPost(sprintf('repositories/%s/node/rename', $repositoryId), $params);
  }
}
