<?php

/*
 * This file is part of the beanstalk-api package.
 *
 * (c) Mickey Freeman <mickey.freeman@eimchicago.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Beanstalk\API;

use Buzz\Message\RequestInterface;
use Buzz\Client\ClientInterface as BuzzClientInterface;
use Beanstalk\API\Http\ClientInterface;
use Beanstalk\API\Http\Client;
use Buzz\Client\Curl;

/**
 * Api
 *
 * @author Mickey Freeman <mickeyfmann@gmail.com>
 */
class Api
{
  /**
   * Api response codes.
   */
  const HTTP_RESPONSE_OK              = 200;
  const HTTP_RESPONSE_CREATED         = 201;
  const HTTP_RESPONSE_NO_CONTENT      = 204;
  const HTTP_RESPONSE_BAD_REQUEST     = 400;
  const HTTP_RESPONSE_UNAUTHORIZED    = 401;
  const HTTP_RESPONSE_FORBIDDEN       = 403;
  const HTTP_RESPONSE_NOT_FOUND       = 404;
  
  /**
   * Account Holder String, needed for your base url
   * @var type 
   */
  protected $_account;
  
  /**
   * Client Interface
   * @var ClientInterface
   */
  protected $_client;
  
  /**
   * Authentication Object
   * @var Authentication\AuthenticationInterface
   */
  protected $_auth;


  public function __construct(BuzzClientInterface $client = null)
  {
    $this->_client = new Client(array(), $client);
    
    return $this;
  } // end function __construct()
  
  public function getClient()
  {
    return $this->_client;
  } // end function getClient()
  
  public function setClient(ClientInterface $client)
  {
    $this->_client = $client;
  } // end function setClient()
  
  public function setAuthentication(Authentication\AuthenticationInterface $auth)
  {
    $this->_auth = $auth;
    $this->getClient()->addListener(
        new Http\Listener\BasicAuthListener($auth->getUsername(), $auth->getPassword())
      );
  } // end function setAuthentication()
  
  public function setAccount($account)
  {
    $this->_account = $account;
    $this->_client->setApiBaseUrl(sprintf("https://%s.beanstalkapp.com/api", $account));
  }
  
  public function getAccount()
  {
    return $this->_account;
  }
  
  public function getAuthentication()
  {
    return $this->_auth;
  }
  
  public function requestGet($endpoint, $params = array(), $headers = array())
  {
    return $this->getClient()->get($endpoint, $params, $headers);
  }
  
  public function requestPost($endpoint, $params = array(), $headers = array())
  {
    return $this->getClient()->post($endpoint, $params, $headers);
  }
  
  public function requestPut($endpoint, $params = array(), $headers = array())
  {
    return $this->getClient()->put($endpoint, $params, $headers);
  }
  
  public function requestDelete($endpoint, $params = array(), $headers = array())
  {
    return $this->getClient()->delete($endpoint, $params, $headers);
  }
  
  protected function _doRequest($method, $endpoint, $params = array(), $headers = array())
  {
    return $this->getClient()->request($endpoint, $params, $method, $headers);
  } // end function request()
  
  public function setFormat($name)
  {
    $this->getClient()->setResponseFormat($name);
    
    return $this;
  } // end function setFormat()
  
  public function getFormat()
  {
    return $this->getClient()->getResponseFormat();
  } // end function getFormat()
}
