<?php

/**
 * This file is part of the beanstalk-api package.
 *
 * (c) Mickey Freeman <mickey.freeman@eimchicago.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Beanstalk\API;

use Buzz\Client\ClientInterface as BuzzClientInterface;

/**
 * @author  Mickey Freeman <mickey.freeman@eimchicago.com>
 */
class Repository extends Api
{  
  public function findAll($page = null, $perPage = null)
  {
    $addOn = null;
    if ($page !== null || $perPage !== null) {
      $page = intval($page);
      $perPage = intval($perPage) > 50 ? 50 : intval($perPage);
      $addOn = sprintf('?page=%s&per_page=%s', $page, $perPage);
    }
    
    return $this->requestGet('repositories.' . $this->getFormat() . $addOn);
  }
  
  public function find($repositoryId)
  {
    return $this->requestGet('repositories/' . sprintf('%s', $repositoryId));
  }
  
  public function findBranches($repositoryId)
  {
    return $this->requestGet('repositories/' . sprintf('%s', $repositoryId) . '/branches');
  }
  
  public function findTags($repositoryId)
  {
    return $this->requestGet('repositories/' . sprintf('%s', $repositoryId) . '/tags');
  }
  
  public function create(array $params)
  {    
    return $this->requestPost('repositories', array('repository' => $params));
  }
  
  public function update($repositoryId, array $params)
  {    
    return $this->requestPut('repositories/' . sprintf('%s', $repositoryId), array('repository' => $params));
  }
}
