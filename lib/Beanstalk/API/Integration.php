<?php

/**
 * This file is part of the beanstalk-api package.
 *
 * (c) Mickey Freeman <mickey.freeman@eimchicago.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Beanstalk\API;

/**
 * @author  Mickey Freeman <mickey.freeman@eimchicago.com>
 */
class Integration extends Api
{  
  public function find($repositoryId, $integrationId = null)
  {
    $endpoint = sprintf('repositories/%s/integrations', $repositoryId);
    if ($integrationId !== null) {
      $endpoint .= sprintf('/%s', $integrationId);
    }
    
    return $this->requestGet($endpoint);
  }
  
  public function create($repositoryId, array $params)
  {
    $endpoint = sprintf('repositories/%s/integrations', $repositoryId);
    return $this->requestPost($endpoint, array('integration' => $params));
  }
  
  public function update($repositoryId, $integrationId, array $params)
  {
    $endpoint = sprintf('repositories/%s/integrations/%s', $repositoryId, $integrationId);
    return $this->requestPut($endpoint, array('integration' => $params));
  }
  
  public function activate($repositoryId, $integrationId)
  {
    return $this->requestPut(sprintf('repositories/%s/integrations/%s/activate', $repositoryId, $integrationId));
  }
  
  public function deactivate($repositoryId, $integrationId)
  {
    return $this->requestPut(sprintf('repositories/%s/integrations/%s/deactivate', $repositoryId, $integrationId));
  }
  
  public function delete($repositoryId, $integrationId)
  {
    return $this->requestDelete(sprintf('repositories/%s/integrations/%s', $repositoryId, $integrationId));
  }
}
