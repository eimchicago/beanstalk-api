<?php

/**
 * This file is part of the beanstalk-api package.
 *
 * (c) Mickey Freeman <mickey.freeman@eimchicago.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Beanstalk\API;

use Buzz\Client\ClientInterface as BuzzClientInterface;

/**
 * @author  Mickey Freeman <mickey.freeman@eimchicago.com>
 */
class Environment extends Api
{  
  public function find($repositoryId, $environmentId = null)
  {
    $endpoint = sprintf('%s/server_environments', $repositoryId);
    if ($environmentId !== null) {
      $endpoint .= sprintf('/%s', $environmentId);
    }
    
    return $this->requestGet($endpoint);
  }
  
  public function create($repositoryId, array $params)
  {    
    $endpoint = sprintf('%s/server_environments', $repositoryId);
    return $this->requestPost($endpoint, array('server_environment' => $params));
  }
  
  public function update($repositoryId, $environmentId, array $params)
  {    
    $endpoint = sprintf('%s/server_environments/%s', $repositoryId, $environmentId);
    return $this->requestPut($endpoint, array('server_environment' => $params));
  }  
}
