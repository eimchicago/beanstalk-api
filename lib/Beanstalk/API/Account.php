<?php

/**
 * This file is part of the beanstalk-api package.
 *
 * (c) Mickey Freeman <mickey.freeman@eimchicago.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Beanstalk\API;

use Buzz\Client\ClientInterface as BuzzClientInterface;

/**
 * @author  Mickey Freeman <mickey.freeman@eimchicago.com>
 */
class Account extends Api
{  
  public function find()
  {
    return $this->requestGet('account');
  } // end function find()
  
  public function update($params = array())
  {    
    return $this->requestPut('account', array('account' => $params));
  } // end function update()
}
