<?php

/**
 * This file is part of the beanstalk-api package.
 *
 * (c) Mickey Freeman <mickey.freeman@eimchicago.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Beanstalk\API;

use Buzz\Client\ClientInterface as BuzzClientInterface;

/**
 * @author  Mickey Freeman <mickey.freeman@eimchicago.com>
 */
class Team extends Api
{  
  public function find()
  {
    return $this->requestGet('teams');
  }
  
  public function create(array $params)
  {    
    return $this->requestPost('teams', $params);
  }
  
  public function update($teamId, array $params)
  {    
    return $this->requestPut(sprintf('teams/%s', $teamId), $params);
  }
  
  public function delete($teamId)
  {
    return $this->requestDelete(sprintf('teams/%s', $teamId));
  }
  
  public function findUsers($teamId)
  {
    return $this->requestGet(sprintf('teams/%s/users', $teamId));
  }
  
  public function addUser($teamId, $userId)
  {
    return $this->requestPut(sprintf('teams/%s/users', $teamId), array('user_id' => $userId));
  }
  
  public function deleteUser($teamId, $userId)
  {
    return $this->requestDelete(sprintf('teams/%s/users/%s', $teamId, $userId));
  }
}
