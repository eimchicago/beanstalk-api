<?php

/**
 * This file is part of the beanstalk-api package.
 *
 * (c) Mickey Freeman <mickey.freeman@eimchicago.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Beanstalk\API;

use Buzz\Client\ClientInterface as BuzzClientInterface;

/**
 * @author  Mickey Freeman <mickey.freeman@eimchicago.com>
 */
class Permissions extends Api
{  
  public function find($userId)
  {
    return $this->requestGet('permissions/' . sprintf('%s', $userId));
  }
  
  public function create(array $params) 
  {
    return $this->requestPost('permissions', array('permission' => $params));
  }
  
  public function delete($permissionId)
  {
    return $this->requestDelete('permissions/' . sprintf('%s', $permissionId));
  }
}
