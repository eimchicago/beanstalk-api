<?php

/**
 * This file is part of the beanstalk-api package.
 *
 * (c) Mickey Freeman <mickey.freeman@eimchicago.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Beanstalk\API;

use Buzz\Client\ClientInterface as BuzzClientInterface;

/**
 * @author  Mickey Freeman <mickey.freeman@eimchicago.com>
 */
class Server extends Api
{  
  public function findAll($repositoryId, $environmentId)
  {
    $endpoint = sprintf('%s/release_servers.', $repositoryId) . $this->getFormat()
              . sprintf('?environment_id=%s', $environmentId);
    return $this->requestGet($endpoint);
  }
  
  public function find($repositoryId, $serverId)
  {
    return $this->requestGet(sprintf('%s/release_servers/%s', $repositoryId, $serverId));
  }
  
  public function create($repositoryId, $environmentId, array $params)
  {
    $endpoint = sprintf('%s/release_servers.', $repositoryId) . $this->getFormat()
              . sprintf('?environment_id=%s', $environmentId);
    return $this->requestPost($endpoint, array('release_server' => $params));
  }
  
  public function update($repositoryId, $serverId, array $params)
  {    
    $endpoint = sprintf('%s/release_servers/%s', $repositoryId, $serverId);
    return $this->requestPut($endpoint, array('release_server' => $params));
  }
  
  public function delete($repositoryId, $serverId)
  {
    return $this->requestDelete(sprintf('%s/release_servers/%s', $repositoryId, $serverId));
  }
}
