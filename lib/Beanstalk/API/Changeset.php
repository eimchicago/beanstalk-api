<?php

/**
 * This file is part of the beanstalk-api package.
 *
 * (c) Mickey Freeman <mickey.freeman@eimchicago.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Beanstalk\API;

/**
 * @author  Mickey Freeman <mickey.freeman@eimchicago.com>
 */
class Changeset extends Api
{
  public function findAll($repositoryId = null, $page = null, $perPage = null, $orderField = 'time', $order = 'DESC')
  {
    if ($repositoryId === null) {
      $endpoint = 'changesets.' . $this->getFormat();
    } else {
      $endpoint = sprintf('changesets/repository.' . $this->getFormat() . '?repository_id=%s', $repositoryId);
    }
    
    $addOns = array();    
    if ($page !== null || $perPage !== null) {
      $page = intval($page);
      $perPage = intval($perPage) > 30 ? 30 : intval($perPage);
      array_push($addOns, sprintf('page=%s&per_page=%s', $page, $perPage));
    }
    
    array_push($addOns, sprintf('order_field=%s&order=%s', $orderField, $order));
    $count = 0;
    foreach ($addOns as $addOn) {
      $endpoint .= ($count == 0 && $repositoryId === null) ? '?' . $addOn : '&' . $addOn;
      $count++;
    } // end foreach addon
    
    return $this->requestGet($endpoint);
  }
  
  public function find($repositoryId, $revision)
  {
    $endpoint = sprintf('changesets/%s.' . $this->getFormat() . '?repository_id=%s', $revision, $repositoryId);
    return $this->requestGet($endpoint);
  }
  
  public function findDiffs($repositoryId, $revision)
  {
    $endpoint = 'changesets/%s/differences.' . $this->getFormat() . '?repository_id=%s';
    return $this->requestGet(sprintf($endpoint, $revision, $repositoryId));
  }
}
