<?php

/**
 * This file is part of the beanstalk-api package.
 *
 * (c) Mickey Freeman <mickey.freeman@eimchicago.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Beanstalk\API;

use Buzz\Client\ClientInterface as BuzzClientInterface;

/**
 * @author  Mickey Freeman <mickey.freeman@eimchicago.com>
 */
class Invitation extends Api
{    
  public function find($invitationId = null)
  {
    if ($invitationId === null) {
      $endpoint = 'invitations';
    } else {
      $endpoint = 'invitations/' . sprintf('%s', $invitationId);
    } // end if invitationId
    
    return $this->requestGet($endpoint);
  }
  
  public function create($name, $email)
  {    
    $user = array('name'  => $name,
                  'email' => $email);
    
    return $this->requestPost('invitations', array('invitation' => array('user' => $user)));
  }
  
  public function resend($userId)
  {
    return $this->requestPut('invitations/' . sprintf('%s', $userId) . '/resend');
  }
}
