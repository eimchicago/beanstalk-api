<?php

/**
 * This file is part of the beanstalk-api package.
 *
 * (c) Mickey Freeman <mickey.freeman@eimchicago.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Beanstalk\API;

use Buzz\Client\ClientInterface as BuzzClientInterface;

/**
 * @author  Mickey Freeman <mickey.freeman@eimchicago.com>
 */
class User extends Api
{  
  public function findAll($page = null, $perPage = null)
  {
    $addOn = null;
    if ($page !== null || $perPage !== null) {
      $page = intval($page);
      $perPage = intval($perPage) > 50 ? 50 : intval($perPage);
      $addOn = sprintf('?page=%s&per_page=%s', $page, $perPage);
    } 
    
    return $this->requestGet('users.' . $this->getFormat() . $addOn);
  }
  
  public function find($userId)
  {
    return $this->requestGet('users/' . sprintf('%s', $userId));
  } // end function find()
  
  public function findCurrent()
  {
    return $this->requestGet('users/current');
  }
  
  public function create($userData = array())
  {    
    return $this->requestPost('users', array('user' => $userData));
  }
  
  public function update($userId, $userData = array())
  {    
    return $this->requestPut('users/' . sprintf('%s', $userId), array('user' => $userData));
  }
  
  public function delete($userId)
  {
    return $this->requestDelete('users/' . sprintf('%s', $userId));
  }
}
