<?php

/**
 * This file is part of the beanstalk-api package.
 *
 * (c) Mickey Freeman <mickey.freeman@eimchicago.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Beanstalk\API;

/**
 * @author  Mickey Freeman <mickey.freeman@eimchicago.com>
 */
class Plan extends Api
{
  public function find()
  {
    return $this->requestGet('plans');
  }
}
