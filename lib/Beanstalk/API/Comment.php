<?php

/**
 * This file is part of the beanstalk-api package.
 *
 * (c) Mickey Freeman <mickey.freeman@eimchicago.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Beanstalk\API;

use Buzz\Client\ClientInterface as BuzzClientInterface;

/**
 * @author  Mickey Freeman <mickey.freeman@eimchicago.com>
 */
class Comment extends Api
{  
  public function findAll($repositoryId, $page = null, $perPage = null)
  {
    $endpoint = sprintf('%s/comments.' . $this->getFormat(), $repositoryId);
    return $this->requestGet($endpoint . $this->_getPaginationString($page, $perPage, '?'));
  }
  
  public function findForChangeset($repositoryId, $revision, $page = null, $perPage = null)
  {
    $endpoint = sprintf('%s/comments.' . $this->getFormat() . '?revision=%s', $repositoryId, $revision);
    return $this->requestGet($endpoint . $this->_getPaginationString($page, $perPage));
  }
  
  public function findForUser($userId, $page = null, $perPage = null)
  {
    $endpoint = sprintf('comments/user.' . $this->getFormat() . '?user_id=%s', $userId);
    return $this->requestGet($endpoint . $this->_getPaginationString($page, $perPage));
  }
  
  public function find($repositoryId, $commentId)
  {
    return $this->requestGet(sprintf('%s/comments/%s', $repositoryId, $commentId));
  }
  
  public function create($repositoryId, array $params)
  {
    return $this->requestPost(sprintf('%s/comments', $repositoryId), array('comment' => $params));
  }
  
  private function _getPaginationString($page, $perPage, $operator = '&')
  {
    if ($page === null && $perPage == null) {
      return null;
    }
    
    $page = intval($page);
    $perPage = intval($perPage) > 50 ? 50 : intval($perPage);
    
    return sprintf($operator . 'page=%s&per_page=%s', $page, $perPage);
  }
}
