<?php

/**
 * This file is part of the beanstalk-api package.
 *
 * (c) Mickey Freeman <mickey.freeman@eimchicago.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Beanstalk\API;

use Buzz\Client\ClientInterface as BuzzClientInterface;

/**
 * @author  Mickey Freeman <mickey.freeman@eimchicago.com>
 */
class PublicKey extends Api
{    
  public function find($keyId)
  {
    return $this->requestGet('public_keys/' . sprintf('%s', $keyId));
  } // end function find
  
  public function findAll($userId = null)
  {
    $endpoint = 'public_keys';
    if ($userId !== null) {
      $endpoint .= '.' . $this->getFormat() . sprintf('?user_id=%s', $userId);
    } // end if userId
    
    return $this->requestGet($endpoint);
  } // end function findAll()
  
  public function create($params = array()) 
  {
    return $this->requestPost('public_keys', array('public_key' => $params));
  }
  
  public function update($keyId, $params = array())
  {    
    return $this->requestPut('public_keys/' . sprintf('%s', $keyId), array('public_key' => $params));
  }
  
  public function delete($keyId)
  {
    return $this->requestDelete(sprintf('public_keys/%s', $keyId));
  }
}
