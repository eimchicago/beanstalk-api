<?php

/**
 * This file is part of the beanstalk-api package.
 *
 * (c) Mickey Freeman <mickey.freeman@eimchicago.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Beanstalk\API;

use Buzz\Client\ClientInterface as BuzzClientInterface;

/**
 * @author  Mickey Freeman <mickey.freeman@eimchicago.com>
 */
class Import extends Api
{  
  public function find($importId = null)
  {
    if ($importId === null) {
      $endpoint = 'repository_imports';
    } else {
      $endpoint = 'repository_imports/' . sprintf('%s', $importId);
    }
    
    return $this->requestGet($endpoint);
  }
  
  public function create($repositoryId, array $params)
  {    
    $endpoint = sprintf('%s/repository_imports', $repositoryId); 
    return $this->requestPost($endpoint, array('repository_import' => $params));
  }
  
  
}
