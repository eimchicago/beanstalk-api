<?php

namespace Beanstalk\Tests\API;

use Beanstalk\API;

class ApiTest extends TestCase
{
  /**
   *
   * @var Beanstalk\API\Api
   */
  protected $_api;
  
  protected function setUp()
  {
    $this->_api = $this->getApiMock('Beanstalk\API\Api');
    parent::setUp();
  }
  
  protected function tearDown()
  {
    unset($this->_api);
    parent::tearDown();
  }
  
  public function testAuthentication()
  {
    $auth = new API\Authentication\Basic('api_username', 'api_password');
    $this->assertInstanceOf('Beanstalk\API\Authentication\Basic', $auth);
    
    $api = new API\Api();
    $api->setAuthentication($auth);
    $this->assertInstanceOf('Beanstalk\API\Authentication\Basic', $api->getAuthentication());
  } // end function testAuthentication()
  
  public function testAccount()
  {
    $account = 'test';
    $expectedBase = sprintf("https://%s.beanstalkapp.com/api", $account);
    
    $api = new API\Api();
    $api->setAccount($account);
    
    $this->assertEquals($account, $api->getAccount());
    $this->assertEquals($expectedBase, $api->getClient()->getApiBaseUrl());
  } // end function testAccount()
  
  /**
   * @expectedException \InvalidArgumentException
   */
  public function testFormat()
  {    
    $api = new API\Api();
    $this->assertEquals('json', $api->getFormat());
    
    $api->setFormat('xml');
    $this->assertEquals('xml', $api->getFormat());
    
    $api->setFormat('invalid');
  }  
}
