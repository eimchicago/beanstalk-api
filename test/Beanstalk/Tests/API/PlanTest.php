<?php

namespace Beanstalk\Tests\API;

use Beanstalk\API;

class PlanTest extends TestCase
{
  protected $_plan;
  
  protected function setUp()
  {
    $this->_plan = $this->getApiMock('Beanstalk\API\Plan');
    parent::setUp();
  }
  
  protected function tearDown()
  {
    unset($this->_plan);
    parent::tearDown();
  }

  public function testFind()
  {
    $endpoint = 'plans';
    
    $this->_plan->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_plan->find();
  }
}
