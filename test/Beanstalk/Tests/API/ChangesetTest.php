<?php

namespace Beanstalk\Tests\API;

use Beanstalk\API;

class ChangesetTest extends TestCase
{
  /**
   *
   * @var Beanstalk\API\Changeset
   */
  protected $_changeset;
  
  public function setUp()
  {
    $this->_changeset = $this->getApiMock('Beanstalk\API\Changeset');
    parent::setUp();
  }
  
  public function tearDown()
  {
    unset($this->_changeset);
    parent::tearDown();
  }
  
  public function testFindAll()
  {
    $endpoint = 'changesets.json?order_field=time&order=DESC';
    
    $this->_changeset->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_changeset->findAll();
  }
  
  public function testFindAllWithId()
  {
    $endpoint = 'changesets/repository.json?repository_id=34&order_field=time&order=DESC';
    
    $this->_changeset->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_changeset->findAll(34);
  }
  
  public function testFindAllWithOptions()
  {
    $endpoint = 'changesets.json?page=2&per_page=20&order_field=time&order=DESC';
    
    $this->_changeset->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_changeset->findAll(null, 2, 20);
  }
  
  public function testFind()
  {
    $endpoint = 'changesets/345.json?repository_id=44';
    
    $this->_changeset->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_changeset->find(44, 345);
  }
  
  public function testFindDiffs()
  {
    $endpoint = 'changesets/345/differences.json?repository_id=44';
    
    $this->_changeset->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_changeset->findDiffs(44, 345);
  }
}
