<?php

namespace Beanstalk\Tests\API;

use Beanstalk\API;

class AccountTest extends TestCase
{
  /**
   *
   * @var Beabstalk\API\Account
   */
  protected $_account = null;
  
  protected function setUp()
  {
    $this->_account = $this->getApiMock('Beanstalk\API\Account');
    parent::setUp();
  }
  
  protected function tearDown()
  {
    unset($this->_account);
    parent::tearDown();
  }
  
  public function testFind()
  {
    $endpoint = 'account';
   
    $this->_account->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_account->find();
  }
  
  public function testUpdate()
  {
    $endpoint = 'account';
    $params = array('name' => 'test');
   
    $this->_account->expects($this->once())
      ->method('requestPut')
      ->with($endpoint, array('account' => $params));
    
    $this->_account->update($params);
  }
}
