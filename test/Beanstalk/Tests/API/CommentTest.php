<?php

namespace Beanstalk\Tests\API;

use Beanstalk\API;

class CommentTest extends TestCase
{
  /**
   *
   * @var Beanstalk\API\Comment
   */
  protected $_comment;
  
  public function setUp()
  {
    $this->_comment = $this->getApiMock('Beanstalk\API\Comment');
    parent::setUp();
  }
  
  public function tearDown()
  {
    unset($this->_comment);
    parent::tearDown();
  }
  
  public function testFindAll()
  {
    $endpoint = '345/comments.json';
    
    $this->_comment->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_comment->findAll(345);
  }
  
  public function testFindAllWithOptions()
  {
    $endpoint = '345/comments.json?page=2&per_page=20';
    
    $this->_comment->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_comment->findAll(345, 2, 20);
  }
  
  public function testFindForChangeset()
  {
    $endpoint = '345/comments.json?revision=500';
    
    $this->_comment->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_comment->findForChangeset(345, 500);
  }
  
  public function testFindForChangesetWithOptions()
  {
    $endpoint = '345/comments.json?revision=500&page=4&per_page=30';
    
    $this->_comment->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_comment->findForChangeset(345, 500, 4, 30);
  }
  
  public function testFindForUser()
  {
    $endpoint = 'comments/user.json?user_id=55';
    
    $this->_comment->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_comment->findForUser(55);
  }
  
  public function testFindForUserWithOptions()
  {
    $endpoint = 'comments/user.json?user_id=55&page=4&per_page=40';
    
    $this->_comment->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_comment->findForUser(55, 4, 40);
  }
  
  public function testFind()
  {
    $endpoint = '345/comments/15';
    
    $this->_comment->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_comment->find(345, 15);
  }
  
  public function testCreate()
  {
    $endpoint = '345/comments';
    $params = array('body'     => 'Test 12',
                    'revision' => '7ada704eac04075f763f36190716e43381fc6e1d');
    
    $this->_comment->expects($this->once())
      ->method('requestPost')
      ->with($endpoint, array('comment' => $params));
    
    $this->_comment->create(345, $params);
  }
}