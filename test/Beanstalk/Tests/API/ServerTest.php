<?php

namespace Beanstalk\Tests\API;

use Beanstalk\API;

class ServerTest extends TestCase
{
  /**
   *
   * @var Beanstalk\API\Server
   */
  protected $_server;
  
  public function setUp()
  {
    $this->_server = $this->getApiMock('Beanstalk\API\Server');
    parent::setUp();
  }
  
  public function tearDown()
  {
    unset($this->_server);
    parent::tearDown();
  }
  
  public function testFindAll()
  {
    $endpoint = '345/release_servers.json?environment_id=15';
    
    $this->_server->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_server->findAll(345, 15);
  }
  
  public function testFind()
  {
    $endpoint = '345/release_servers/45';
    
    $this->_server->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_server->find(345, 45);
  }
  
  public function testCreate()
  {
    $endpoint = '345/release_servers.json?environment_id=15';
    $params = array('authenticate_by_key' => false,
                    'name'                => "Eim Staging",
                    'use_feat'            => false,
                    'remote_path'         => 'tmp/bla',
                    'port'                => 22,
                    'protocol'            => 'sftp',
                    'local_path'          => '/',
                    'use_active_mode'     => false,
                    'remote_addr'         => 'staging.envisionitmedia.com',
                    'pre_release_hook'    => null,
                    'post_release_hook'   => null);
    
    $this->_server->expects($this->once())
      ->method('requestPost')
      ->with($endpoint, array('release_server' => $params));
    
    $this->_server->create(345, 15, $params);
  }
  
  public function testUpdate()
  {
    $endpoint = '345/release_servers/15';
    $params = array('name' => 'moon');
    
    $this->_server->expects($this->once())
      ->method('requestPut')
      ->with($endpoint, array('release_server' => $params));
    
    $this->_server->update(345, 15, $params);
  }
  
  public function testDelete()
  {
    $endpoint = '345/release_servers/15';
    
    $this->_server->expects($this->once())
      ->method('requestDelete')
      ->with($endpoint);
    
    $this->_server->delete(345, 15);
  }
}
