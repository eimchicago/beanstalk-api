<?php

namespace Beanstalk\Tests\API;

use Beanstalk\API;

class RepositoryTest extends TestCase
{
  /**
   *
   * @var Beanstalk\API\Repository
   */
  protected $_repository;
  
  protected function setUp()
  {
    $this->_repository = $this->getApiMock('Beanstalk\API\Repository');
    parent::setUp();
  }
  
  public function testFindAll()
  {
    $endpoint = 'repositories.json';
    
    $this->_repository->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_repository->findAll();
  }
  
  public function testFindAllWithOptions()
  {
    $endpoint = 'repositories.json?page=2&per_page=20';
    
    $this->_repository->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_repository->findAll(2, 20);
  }
  
  public function testFind()
  {
    $endpoint = 'repositories/123';
    $expected = json_encode('dummy');
    
    $this->_repository->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_repository->find(123);
  }
  
  public function testFindBranches()
  {
    $endpoint = 'repositories/123/branches';
    
    $this->_repository->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_repository->findBranches(123);
  }
  
  public function testFindTags()
  {
    $endpoint = 'repositories/123/tags';
    
    $this->_repository->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_repository->findTags(123);
  }
  
  public function testCreate()
  {
    $endpoint = 'repositories';
    $params = array('title' => 'test',
                    'name'  => 'test',
                    'color_label' => 'label-blue',
                    'type_id'     => 'git');
    
    $this->_repository->expects($this->once())
      ->method('requestPost')
      ->with($endpoint, array('repository' => $params));
    
    $this->_repository->create($params);
  }
  
  public function testUpdate()
  {
    $endpoint = 'repositories/123';
    $params = array('name' => 'new-name');
    
    $this->_repository->expects($this->once())
      ->method('requestPut')
      ->with($endpoint, array('repository' => $params));
    
    $this->_repository->update('123', $params);
  }
}