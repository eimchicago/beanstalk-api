<?php

namespace Beanstalk\Tests\API;

use Beanstalk\API;

class UserTest extends TestCase
{
  /**
   *
   * @var Beanstalk\API\User
   */
  protected $_user;
  
  protected function setUp()
  {
    $this->_user = $this->getApiMock('Beanstalk\API\User');
    parent::setUp();
  }
  
  protected function tearDown()
  {
    unset($this->_user);
    parent::tearDown();
  }
  
  public function testFindAll()
  {
    $endpoint = 'users.json';
    
    $this->_user->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_user->findAll();
  }
  
  public function testFindAllWithOptions()
  {
    $endpoint = 'users.json?page=2&per_page=40';
    
    $this->_user->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_user->findAll(2, 40);
  }
  
  public function testFindCurrent()
  {
    $endpoint = 'users/current';
    
    $this->_user->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_user->findCurrent();
  }
  
  public function testCreate()
  {
    $endpoint = 'users';
    $params = array('admin'    => false,
                    'timezone' => 'Philadelphia',
                    'name'     => 'John Doe', 
                    'login'    => 'jdoe',
                    'email'    => 'jdoe@test.com',
                    'password' => 'test23w43');
    
    $this->_user->expects($this->once())
      ->method('requestPost')
      ->with($endpoint, array('user' => $params));
    
    $this->_user->create($params);
  }
  
  public function testUpdate()
  {
    $endpoint = 'users/123';
    $params = array('name' => 'Jane Doe');
    
    $this->_user->expects($this->once())
      ->method('requestPut')
      ->with($endpoint, array('user' => $params));
    
    $this->_user->update(123, $params);
  }
  
  public function testDelete()
  {
    $endpoint = 'users/123';
    
    $this->_user->expects($this->once())
      ->method('requestDelete')
      ->with($endpoint);
    
    $this->_user->delete(123);
  }
}
