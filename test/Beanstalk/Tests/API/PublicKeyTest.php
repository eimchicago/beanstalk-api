<?php

namespace Beanstalk\Tests\API;

use Beanstalk\API;

class PublicKeyTest extends TestCase
{
  /**
   *
   * @var Beanstalk\API\PublicKey
   */
  protected $_publicKey;
  
  protected function setUp()
  {
    $this->_publicKey = $this->getApiMock('Beanstalk\API\PublicKey');
    parent::setUp();
  }
  
  protected function tearDown()
  {
    unset($this->_publicKey);
    parent::tearDown();
  }
  
  public function testFind()
  {
    $endpoint = 'public_keys/3653';
    
    $this->_publicKey->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_publicKey->find(3653);
  }
  
  public function testFindAll()
  {
    $endpoint = 'public_keys';
    
    $this->_publicKey->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_publicKey->findAll();
  }
  
  public function testFindAllForUser()
  {
    $endpoint = 'public_keys.json?user_id=312';
    
    $this->_publicKey->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_publicKey->findAll(312);
  }
  
  public function testCreate()
  {
    $endpoint = 'public_keys';
    $params = array('content' => 'blabla',
                    'name'    => 'test key');
    
    $this->_publicKey->expects($this->once())
      ->method('requestPost')
      ->with($endpoint, array('public_key' => $params));
    
    $this->_publicKey->create($params);
  }
  
  public function testUpdate()
  {
    $endpoint = 'public_keys/32';
    $params = array('name' => 'new key');
    
    $this->_publicKey->expects($this->once())
      ->method('requestPut')
      ->with($endpoint, array('public_key' => $params));
    
    $this->_publicKey->update(32, $params);
  }
  
  public function testDelete()
  {
    $endpoint = 'public_keys/32';
    
    $this->_publicKey->expects($this->once())
      ->method('requestDelete')
      ->with($endpoint);
    
    $this->_publicKey->delete(32);
  }
}
