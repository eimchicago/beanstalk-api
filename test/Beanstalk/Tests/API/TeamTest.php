<?php

namespace Beanstalk\Tests\API;

use Beanstalk\API;

class TeamTest extends TestCase
{
  /**
   *
   * @var Beanstalk\API\Team
   */
  protected $_team;
  
  public function setUp()
  {
    $this->_team = $this->getApiMock('Beanstalk\API\Team');
    parent::setUp();
  }
  
  public function tearDown()
  {
    unset($this->_team);
    parent::tearDown();
  }
  
  public function testFind()
  {
    $endpoint = 'teams';
    
    $this->_team->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_team->find();
  }
  
  public function testCreate()
  {
    $endpoint = 'teams';
    $params = array('name'        => 'Testers',
                    'color_label' => 'red',
                    'users'       => array(3),
                    'permissions' => array('1' => array('write'                 => true,
                                                        'deploy'                => true,
                                                        'configure_deployments' => true),
                                           '2' => array('write'                 => true,
                                                        'deploy'                => true,
                                                        'configure_deployments' => false)));
    
    $this->_team->expects($this->once())
      ->method('requestPost')
      ->with($endpoint, $params);
    
    $this->_team->create($params);
  }
  
  public function testUpdate()
  {
    $endpoint = 'teams/34';
    $params = array('permissions' => array('1' => array('write'                 => true,
                                                        'deploy'                => true,
                                                        'configure_deployments' => true),
                                           '3' => array('write'                 => true,
                                                        'deploy'                => false,
                                                        'configure_deployments' => false)),
                    'color_label' => 'green',
                    'users'       => array(3, 4));
    
    $this->_team->expects($this->once())
      ->method('requestPut')
      ->with($endpoint, $params);
    
    $this->_team->update(34, $params);
  }
  
  public function testDelete()
  {
    $endpoint = 'teams/34';
    
    $this->_team->expects($this->once())
      ->method('requestDelete')
      ->with($endpoint);
    
    $this->_team->delete(34);
  }
  
  public function testFindUsers()
  {
    $endpoint = 'teams/34/users';
    
    $this->_team->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_team->findUsers(34);
  }
  
  public function testAddUser()
  {
    $endpoint = 'teams/34/users';
    $params = array('user_id' => 234);
    
    $this->_team->expects($this->once())
      ->method('requestPut')
      ->with($endpoint, $params);
    
    $this->_team->addUser(34, 234);
  }
  
  public function testDeleteUser()
  {
    $endpoint = 'teams/34/users/234';
    
    $this->_team->expects($this->once())
      ->method('requestDelete')
      ->with($endpoint);
    
    $this->_team->deleteUser(34, 234);
  }
}
