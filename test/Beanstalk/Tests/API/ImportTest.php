<?php

namespace Beanstalk\Tests\API;

use Beanstalk\API;

class ImportTest extends TestCase
{
  /**
   *
   * @var Beanstalk\API\Import
   */
  protected $_import;
  
  public function setUp()
  {
    $this->_import = $this->getApiMock('Beanstalk\API\Import');
    parent::setUp();
  }
  
  public function tearDown()
  {
    unset($this->_import);
    parent::tearDown();
  }
  
  public function testFind()
  {
    $endpoint = 'repository_imports';
    
    $this->_import->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_import->find();
  }
  
  public function testFindWithId()
  {
    $endpoint = 'repository_imports/345';
    
    $this->_import->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_import->find(345);
  }
  
  public function testCreate()
  {
    $endpoint = '234/repository_imports';
    $params = array('uri' => 'https://www.envisionitmedia.com');
    
    $this->_import->expects($this->once())
      ->method('requestPost')
      ->with($endpoint, array('repository_import' => $params));
    
    $this->_import->create(234, $params);
  }
}
