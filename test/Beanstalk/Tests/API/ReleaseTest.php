<?php

namespace Beanstalk\Tests\API;

use Beanstalk\API;

class ReleaseTest extends TestCase
{
  /**
   *
   * @var Beanstalk\API\Release
   */
  protected $_release;
  
  public function setUp()
  {
    $this->_release = $this->getApiMock('Beanstalk\API\Release');
    parent::setUp();
  }
  
  public function tearDown()
  {
    unset($this->_release);
    parent::tearDown();
  }
  
  public function testFindAll()
  {
    $endpoint = 'releases';
    
    $this->_release->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_release->findAll();
  }
  
  public function testFindAllWithId()
  {
    $endpoint = '345/releases';
    
    $this->_release->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_release->findAll(345);
  }
  
  public function testFindAllWithOptions()
  {
    $endpoint = 'releases.json?page=4&per_page=30';
    
    $this->_release->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_release->findAll(null, 4, 30);
  }
  
  public function testFind()
  {
    $endpoint = '345/releases/27';
    
    $this->_release->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_release->find(345, 27);
  }
  
  public function testCreate()
  {
    $endpoint = '345/releases.json?environment_id=15';
    $params = array('revision' => 'a77a6bafd54c947c029b90d983e987ff4923b110',
                    'comment'  => 'Deployment description or commit message.');
    
    $this->_release->expects($this->once())
      ->method('requestPost')
      ->with($endpoint, array('release' => $params));
    
    $this->_release->create(345, 15, $params);
  }
  
  public function testRetry()
  {
    $endpoint = '345/releases/35/retry';
    
    $this->_release->expects($this->once())
      ->method('requestPut')
      ->with($endpoint);
    
    $this->_release->retry(345, 35);
  }
}
