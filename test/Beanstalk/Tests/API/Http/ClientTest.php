<?php

namespace Beanstalk\Tests\API\Http;

use Beanstalk\Tests\API as Tests;
use Beanstalk\API\Http\Client;
use Buzz\Client\Curl;

class ClientTest extends Tests\TestCase
{
  private $_client;
  
  public function setUp()
  {
    $this->_client = new Client(array(), new Curl());
  }
  
  public function testGetSelf()
  {
    $this->assertInstanceOf('\Buzz\Client\Curl', $this->_client->getClient());
  }
  
  /**
   * @expectedException \InvalidArgumentException
   */
  public function testSetResponseFormatInvalid()
  {
    $this->_client->setResponseFormat('invalid');
  }
  
  public function testResponseFormatSuccess()
  {
    $this->_client->setResponseFormat('xml');
    $this->assertEquals('xml', $this->_client->getResponseFormat());
  }
  
  public function testSetApiBaseUrl()
  {
    $baseUrl = 'https://test.beanstalkapp.com/api';
    
    $this->assertNotEquals($baseUrl, $this->_client->getApiBaseUrl());
    
    $this->_client->setApiBaseUrl($baseUrl);
    $this->assertEquals($baseUrl, $this->_client->getApiBaseUrl());
  }
  
  public function testGetApiBaseUrl()
  {
    $this->assertEquals('https://beanstalkapp.com/api', $this->_client->getApiBaseUrl());
  }
}