<?php

namespace Beanstalk\Tests\API\Authentication;

use Beanstalk\Tests\API as Tests;
use Beanstalk\API\Authentication;

class BasicTest extends Tests\TestCase
{
  private $_auth;
  
  public function setUp()
  {
    $this->_auth = new Authentication\Basic('api_username', 'api_password');
  }
  
  public function testWrongCredentials()
  {
    $this->assertNotEquals('username', $this->_auth->getUsername());
    $this->assertNotEquals('password', $this->_auth->getPassword());
  }
  
  public function testSetUsername()
  {
    $this->_auth->setUsername('username');
    $this->assertEquals('username', $this->_auth->getUsername());
  }
  
  public function testSetPassword()
  {
    $this->_auth->setPassword('password');
    $this->assertEquals('password', $this->_auth->getPassword());
  }
  
  public function testAuthenticateWrongUsername()
  {
    $request = new \Buzz\Message\Request;
    $this->_auth->authenticate($request);
    $this->_auth->setUsername('wrong_username');
    $this->assertNotEquals('api_username', $this->_auth->getUsername());
  }
  
  public function testAuthenticateWrongPassword()
  {
    $request = new \Buzz\Message\Request;
    $this->_auth->authenticate($request);
    $this->_auth->setPassword('wrong_password');
    
    $this->assertNotEquals('api_password', $this->_auth->getPassword());
  }
  
  public function testAuthenticateSuccess()
  {
    $request = new \Buzz\Message\Request;
    $this->_auth->authenticate($request);
    
    $header = $request->getHeader('Authorization');
    $this->assertNotNull($header);
    
    list(, $credentials) = explode(' ', $header);
    $pcs = explode(':', base64_decode($credentials));
    
    $this->assertEquals('api_username', $pcs[0]);
    $this->assertEquals('api_password', $pcs[1]);
  }
}
