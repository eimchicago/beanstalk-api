<?php

namespace Beanstalk\Tests\API;

use Beanstalk\API;

class EnvironmentTest extends TestCase
{
  /**
   *
   * @var Beanstalk\API\Envirionment
   */
  protected $_environment;
  
  public function setUp()
  {
    $this->_environment = $this->getApiMock('Beanstalk\API\Environment');
    parent::setUp();
  }
  
  public function tearDown()
  {
    unset($this->_environment);
    parent::tearDown();
  }
  
  public function testFind()
  {
    $endpoint = '345/server_environments';
    
    $this->_environment->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_environment->find(345);
  }
  
  public function testFindWithId()
  {
    $endpoint = '345/server_environments/15';
    
    $this->_environment->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_environment->find(345, 15);
  }
  
  public function testCreate()
  {
    $endpoint = '345/server_environments';
    $params = array('branch_name' => 'development',
                    'name'        => 'Staging',
                    'automatic'   => false);
    
    $this->_environment->expects($this->once())
      ->method('requestPost')
      ->with($endpoint, array('server_environment' => $params));
    
    $this->_environment->create(345, $params);
  }
  
  public function testUpdate()
  {
    $endpoint = '345/server_environments/15';
    $params = array('automatic' => false,
                    'name'      => 'Development');
    
    $this->_environment->expects($this->once())
      ->method('requestPut')
      ->with($endpoint, array('server_environment' => $params));
    
    $this->_environment->update(345, 15, $params);
  }
}
