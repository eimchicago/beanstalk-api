<?php

namespace Beanstalk\Tests\API;

use Beanstalk\API;

class PermissionsTest extends TestCase
{
  /**
   *
   * @var Beanstalk\API\Permissions
   */
  protected $_permissions;
  
  public function setUp()
  {
    $this->_permissions = $this->getApiMock('Beanstalk\API\Permissions');
    parent::setUp();
  }
  
  public function tearDown()
  {
    unset($this->_permissions);
    parent::tearDown();
  }
  
  public function testFind()
  {
    $endpoint = 'permissions/45';
    
    $this->_permissions->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_permissions->find(45);
  }
  
  public function testCreate()
  {
    $endpoint = 'permissions';
    $params = array('server_environment_id' => 3434,
                    'user_id'               => 45,
                    'repository_id'         => 23,
                    'write'                 => true);
    
    $this->_permissions->expects($this->once())
      ->method('requestPost')
      ->with($endpoint, array('permission' => $params));
    
    $this->_permissions->create($params);
  }
  
}
