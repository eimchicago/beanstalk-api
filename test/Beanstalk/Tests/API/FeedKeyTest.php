<?php

namespace Beanstalk\Tests\API;

use Beanstalk\API;

class FeedKeyTest extends TestCase
{
  /**
   *
   * @var Beanstalk\API\FeedKey
   */
  protected $_feedKey;
  
  public function setUp()
  {
    $this->_feedKey = $this->getApiMock('Beanstalk\API\FeedKey');
    parent::setUp();
  }
  
  public function tearDown()
  {
    unset($this->_feedKey);
    parent::tearDown();
  }
  
  public function testFind()
  {
    $endpoint = 'feed_key';
    
    $this->_feedKey->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_feedKey->find();
  }
}
