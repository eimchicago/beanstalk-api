<?php

namespace Beanstalk\Tests\API;

use Beanstalk\API;

class IntegrationTest extends TestCase
{
  /**
   *
   * @var Beanstalk\API\Integration
   */
  protected $_integration;
  
  public function setUp()
  {
    $this->_integration = $this->getApiMock('Beanstalk\API\Integration');
    parent::setUp();
  }
  
  public function tearDown()
  {
    unset($this->_integration);
    parent::tearDown();
  }
  
  public function testFind()
  {
    $endpoint = 'repositories/234/integrations';
    
    $this->_integration->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_integration->find(234);
  }
  
  public function testFindWithId()
  {
    $endpoint = 'repositories/234/integrations/23';
    
    $this->_integration->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_integration->find(234, 23);
  }
  
  public function testCreate()
  {
    $endpoint = 'repositories/234/integrations';
    $params = array('service_url' => "http://www.postbin.org/13leoic",
                    'type'        => 'WebHooksIntegration');
    
    $this->_integration->expects($this->once())
      ->method('requestPost')
      ->with($endpoint, array('integration' => $params));
    
    $this->_integration->create(234, $params);
  }
  
  public function testUpdate()
  {
    $endpoint = 'repositories/234/integrations/23';
    $params = array('service_url' => 'http://example.com?webhook');
    
    $this->_integration->expects($this->once())
      ->method('requestPut')
      ->with($endpoint, array('integration' => $params));
    
    $this->_integration->update(234, 23, $params);
  }
  
  public function testActivate()
  {
    $endpoint = 'repositories/345/integrations/34/activate';
    
    $this->_integration->expects($this->once())
      ->method('requestPut')
      ->with($endpoint);
    
    $this->_integration->activate(345, 34);
  }
  
  public function testDeactivate()
  {
    $endpoint = 'repositories/345/integrations/34/deactivate';
    
    $this->_integration->expects($this->once())
      ->method('requestPut')
      ->with($endpoint);
    
    $this->_integration->deactivate(345, 34);
  }
  
  public function testDelete()
  {
    $endpoint = 'repositories/345/integrations/34';
    
    $this->_integration->expects($this->once())
      ->method('requestDelete')
      ->with($endpoint);
    
    $this->_integration->delete(345, 34);
  }
}
