<?php

namespace Beanstalk\Tests\API;

use Beanstalk\API;

class InvitationTest extends TestCase
{
  /**
   *
   * @var Beanstalk\API\Invitation
   */
  protected $_invitation;
  
  protected function setUp()
  {
    $this->_invitation = $this->getApiMock('Beanstalk\API\Invitation');
    parent::setUp();
  }
  
  protected function tearDown()
  {
    unset($this->_invitation);
    parent::tearDown();
  }
  
  public function testFind()
  {
    $endpoint = 'invitations';
    
    $this->_invitation->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_invitation->find();
  }
  
  public function testFindWithId()
  {
    $endpoint = 'invitations/124';
    
    $this->_invitation->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_invitation->find(124);
  }
  
  public function testCreate()
  {
    $endpoint = 'invitations';
    $params = array('email' => 'test@test.com',
                    'name'  => 'Testy McTesterson');
    
    $this->_invitation->expects($this->once())
      ->method('requestPost')
      ->with($endpoint, array('invitation' => array('user' => $params)));
    
    $this->_invitation->create($params['name'], $params['email']);
  }
  
  public function testResend()
  {
    $endpoint = 'invitations/345/resend';
    
    $this->_invitation->expects($this->once())
      ->method('requestPut')
      ->with($endpoint);
    
    $this->_invitation->resend(345);
  }
}
