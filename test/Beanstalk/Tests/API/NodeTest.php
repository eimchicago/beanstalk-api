<?php

namespace Beanstalk\Tests\API;

use Beanstalk\API;

class NodeTest extends TestCase
{
  /**
   *
   * @var Beanstalk\API\Comment
   */
  protected $_node;
  
  public function setUp()
  {
    $this->_node = $this->getApiMock('Beanstalk\API\Node');
    parent::setUp();
  }
  
  public function tearDown()
  {
    unset($this->_node);
    parent::tearDown();
  }
  
  public function testFind()
  {
    $endpoint = 'repositories/345/node.json';
    
    $this->_node->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_node->find(345);
  }
  
  public function testFindWithOptions()
  {
    $endpoint = 'repositories/345/node.json?path=trunk/design&revision=500&size=1&contents=1';
    
    $this->_node->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_node->find(345, 'trunk/design', 500, true, true);
  }
  
  public function testFindBlob()
  {
    $endpoint = 'repositories/345/blob.json?id=3bb0e8592a';
    
    $this->_node->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_node->findBlob(345, '3bb0e8592a');
  }
  
  public function testFindBlobWithOptions()
  {
    $endpoint = 'repositories/345/blob.json?id=3bb0e8592a&name=test.txt';
    
    $this->_node->expects($this->once())
      ->method('requestGet')
      ->with($endpoint);
    
    $this->_node->findBlob(345, '3bb0e8592a', 'test.txt');
  }
  
  public function testCreate()
  {
    $endpoint = 'repositories/345/node';
    $params = array('commit_message' => 'This is a test',
                    'branch'         => 'Master',
                    'path'           => 'src/utils.clj',
                    'contents'       => '(defn- basename [path]\n(.getName (File. ^String path)))\n');
    
    $this->_node->expects($this->once())
      ->method('requestPost')
      ->with($endpoint, $params);
    
    $this->_node->create(345, $params);
  }
  
  public function testUpdate()
  {
    $endpoint = 'repositories/345/node';
    $params = array('commit_message' => 'This is a test',
                    'branch'         => 'Master',
                    'path'           => 'src/utils.clj',
                    'contents'       => '(defn- basename [path]\n(.getName (File. ^String path)))\n');
    
    $this->_node->expects($this->once())
      ->method('requestPut')
      ->with($endpoint, $params);
    
    $this->_node->update(345, $params);
  }
  
  public function testDelete()
  {
    $endpoint = 'repositories/345/node';
    $params = array('commit_message' => 'Removing test file', 
                    'branch'         => 'master',
                    'path'           => 'src/utils.clj');
    
    $this->_node->expects($this->once())
      ->method('requestDelete')
      ->with($endpoint, $params);
    
    $this->_node->delete(345, $params);
  }
  
  public function testRename()
  {
    $endpoint = 'repositories/345/node/rename';
    $params = array('commit_message' => 'Renaming utilitiy functions',
                    'branch'         => 'master',
                    'path'           => 'src/utils.clj',
                    'new_name'       => 'util_functions.clj');
    
    $this->_node->expects($this->once())
      ->method('requestPost')
      ->with($endpoint, $params);
    
    $this->_node->rename(345, $params);
  }
}
